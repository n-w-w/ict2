const path = require("path");
const glob = require("glob")
const webpack = require("webpack");
const CleanWebpackPlugin = require('clean-webpack-plugin');


function toObject (paths) {
  let obj = {}
  paths.forEach((path) => {
    obj[path.split('/').slice(-1)[0]] = path;
  })
  return obj;
}

module.exports = {
  devtool: 'cheap-module-eval-source-map',
  entry: toObject(glob.sync('./public/src/**/*.js*')),
  mode: "development",
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules|bower_components)/,
        loader: "babel-loader",
        options: { presets: ["@babel/env","@babel/react"] }
      },
    ]
  },
  resolve: { extensions: [".js", ".jsx"] },
  output: {
    path: path.resolve(__dirname, "./public/dist"),
    filename: "[name]"
  },
  plugins: [
    new CleanWebpackPlugin(),
    new webpack.SourceMapDevToolPlugin({}),

    //new webpack.HotModuleReplacementPlugin()
  ]
};