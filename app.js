/**
 * Module dependencies.
 */
const http = require('http');
const express = require('express');
const logger = require('morgan');
const path = require('path');
const session = require('express-session');
const bodyParser = require('body-parser');
const config = require('./config')
const wsServer = require('./ws-server');

const Database = require('./api/models/database');
const db = new Database()
const MysqlStore = require('express-mysql-session')(session);

const app = module.exports = express();

app.set('view engine', 'ejs');

// set views for error and 404 pages
app.set('views', path.join(__dirname, 'views'));

if (!module.parent) app.use(logger('dev'));

app.use(express.static(path.join(__dirname, 'public')));

const sessionStore = new MysqlStore({}, db.connection);
const sessionParser = session({
  name: config.session.name,
  resave: false,
  saveUninitialized: false,
  secret: 'some secret here',
  store: sessionStore,
  cookie: {
    httpOnly: false,
    maxAge: null,
  }
})
app.use(sessionParser);


// parse request bodies (req.body)
app.use(bodyParser.json())
app.use(express.urlencoded({ extended: true }));

// load routes
require('./routes')(app);

app.use(function(err, req, res, next){
  if (!module.parent) console.error(err.stack);
  res.status(500).render('5xx');
});

// assume 404 since no middleware responded
app.use(function(req, res, next){
  res.status(404).render('404', { url: req.originalUrl });
});

/* istanbul ignore next */
if (!module.parent) {
  const server = new http.createServer(app).listen(3000);
  wsServer(server, sessionParser);
  console.log('Express started on port 3000');
}
