const WebSocket = require("ws");
const session = require('express-session');
const jwt = require('jsonwebtoken');
const Database = require("./api/models/database");
const db = new Database();
let wsServer;

const Controller = {
    pingBack: function(msg, ws) {
        ws.send({
            routes: '/ping-back',
            content: 'pong'
        })
    },
    sendUser: async function(msg, ws) {
        const sender = await db.helpers.user.findOne({id: ws.userId})
        const receiverId = msg.params[0]
        const receiverWs = wsServer.clients.find((client) => (client.userId == receiverId));
        const msgToSend = {
            senderId: sender.userId,
            content: msg.content,
            type: msg.type,
            datetime: msg.datetime,
            receiverId: receiverId
        }
        receiverWs.send({
            route: '/msg/user/?',
            params: [receiverId],
            msg: msgToSend
        });
        await db.helpers.message.createPersonMsg(msgToSend);
    },
    sendGroup: function(msg, ws) {

    }
}

async function getInitMsg(userId) {
    console.log('#getInitMsg');
    console.log('userId');
    console.log(userId);
    const chatToken = jwt.sign({ userId: userId }, 'shhhhh');
    const chatList = await db.helpers.msg.findChatList({userId});
    let userIds, groupIds;
    chatList.forEach(chat => {
        chat.userId?
            userIds.push(chat.userId)
                :chat.groupId?
                    groupIds.push(chat.groupId) : null;
    });
    const userList = await db.helpers.user.findMany({userIds});
    const groupList = await db.helpers.group.findMany({groupIds});
    const msgList = await db.helpers.msg.findMsgs({userId, otherUserId: 'ANY', groupId: 'ANY'});
    console.log("userList");
    console.dir(userList)
    console.dir(groupList);
    console.dir(msgList);
    return [
        {
            route: '/chat-token',
            msg: chatToken
        },
        {
            route: '/user-list',
            msg: userList
        },
        {
            route: '/group-list',
            msg: groupList
        },
        {
            route: '/msg-list',
            msg: msgList
        },
        {
            route: '/chat-list',
            msg: chatList
        },
    ]

}
  
function handleConnection(ws, req) {

    console.log("connected to ws client");
    console.dir(req.session);
   
    ws.isAlive = true;
    ws.on('pong', function () {
        this.isAlive = true;
    });
    ws.on("msg", handleMsg);
    session(req, {}, () => {
        const userId = req.session.userId;
        ws.userId = userId;
        ws.send(getInitMsg(userId));
    });
}

function handleMsg(msg) {
    const ws = this;
    switch (msg.route) {
        case "/fetch/personal-chat/?":
            break;
        case "/fetch/group-chat/?":
            break;
        case "/fetch/user-info/?":
            break;
        case "/fetch/group-info/?":
            break;
        case "/send/user/?":
            Controller.sendUser(msg, ws);
            break;
        case "/send/group/?":
            Controller.sendGroup(msg, ws);
            break;
        case '/ping':
            Controller.pingBack(msg, ws);
            break;
        default:
            break;
    }
}

function pingClients(clients) {
    clients.forEach((ws) => {
        if (ws.isAlive === false) return ws.terminate();

        ws.isAlive = false;
        ws.ping();
    });
}

module.exports = (httpServer, sessionParser) => {
    wsServer = new WebSocket.Server({
        server: httpServer,
        verifyClient: (info, done) => {
            console.log('verifying client');
            sessionParser(info.req, {}, () => {
                console.log('session parsed!');
                console.dir(info.req.session.userId);
                done(info.req.session.userId);
            })
        }
    });
    wsServer.on('connection', handleConnection);
    
    setInterval(()=>{pingClients(wsServer.clients)}, 30000);
}
