import React from 'react';
import SigninButton from "./SigninButton";

export default function Header() {
    return (
        <div className="header">
            <div className="pure-menu pure-menu-horizontal">
                <a className="pure-menu-heading" href="">ICT 2</a>
                <ul className="pure-menu-list">
                    <li className="pure-menu-item pure-menu-selected"><a href="#" className="pure-menu-link">Home</a></li>
                    <li className="pure-menu-item"><a href="#" className="pure-menu-link">Chat</a></li>
                </ul>
                <SigninButton />
            </div>
        </div>
    );
}
