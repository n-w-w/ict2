const sessionName = require('../../../../../config').session.name;
import React from "react";
import PropTypes from "prop-types";

export default class SigninButton extends React.Component {
    constructor(props) {
        super(props);
        window.handleSignin = this.handleSignin;
    }

    handleSignin(googleUser) {
        var idToken = googleUser.getAuthResponse().id_token;
        console.log(JSON.stringify({idToken: idToken}))
        fetch(window.location.origin+'/tokensignin', {
            method: 'post',
            credentials: 'include',
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({idToken: idToken}),
        }).then((data)=> {
            return JSON.parse(data)
        }).then((data)=> {
            document.cookie = `${sessionName}= ${data.sid}`
        })
    }
    
    render() {
        return <div className="g-signin2" data-onsuccess="handleSignin"></div>;
    }
}