import React from "react";
import ReactDOM from 'react-dom';
import NavBar from '../../generic/components/Header';
import SocketControl from '../components/SocketControl';

class Page extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div className="">
                <NavBar />
                <SocketControl />
            </div>
        )
    }
}

ReactDOM.render(<Page />, document.getElementById('root'));