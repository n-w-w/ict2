import PropTypes from 'prop-types';
import {Route} from 'react-router-dom'

function findMsg() {
    
}

export default function ChatList(props) {
    const {chatList, findData} =  props;
    return (
        <div>
            {chatList.map(chat => {
                const lastMsg = findData('MSG', chat.lastMsgId);
                const user = findData('USER', lastMsg.userId);
                const group = findData('GROUP', lastMsg.groupId);
                const header = user? user.name : group? group.name : null;
                const avatarLink = user? user.avatarLink : group? group.avatarLink : null;
                let lastMsgText;
                switch (lastMsg.contentType) {
                    case 'STRING':
                        lastMsgText = lastMsg.content;
                        break;
                    case 'IMG-LINK':
                        lastMsgText = 'Image';
                        break;
                    case 'VID-LINK':
                        lastMsgText = 'Video';
                        break;
                    case 'FILE-LINK':
                        lastMsgText = 'FILE';
                        break;
                    default:
                        lastMsgText = lastMsg.content;
                        break;
                }
                return (
                    <div className="chat-list-item">
                        <Avatar link={avatarLink} />
                        <div className="header">{header}</div>
                        <div className="last-msg-text">
                            {lastMsgText}
                        </div>
                        <div className="last-msg-datetime">{lastMsg.datetime}</div>
                    </div>
                )
            })}
        </div>
    )
}

ChatList.propTypes = {
    chatList: PropTypes.array,
    findData: PropTypes.func.isRequired
}