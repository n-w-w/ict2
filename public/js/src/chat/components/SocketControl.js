import React from "react";
import ChatControl from './ChatControl';

const arrayToObject = (array) =>
    array.reduce((obj, item) => {
        const id = item.userId
        obj[item.id] = item
        return obj
    }, {})

const DATA = {
    userList: [
        {
            id: 1,
            name: 'Jack',
            avatarLink: '/img/avatar/user/1'
        },
        {
            id: 2,
            name: 'Meme',
            avatarLink: '/img/avatar/user/2'
        }
    ],
    groupList: [
        {
            id: 1,
            name: 'GROUP Bebabs',
            avatarLink: '/img/avatar/group/1'
        }
    ],
    msgList: {
        users: [
            {
                id: 1,
                senderId: 1,
                receiverId: 3, //current user
                content: 'HELLoo',
                contentType: 'STRING',
                datetime: new Date(),
            },
            {
                id: 2,
                senderId: 3, //current user idm
                receiver: 2,
                content: 'Hii',
                contentType: 'STRING',
                datetime: new Date(),
                totalRead: 1
                
            },
        ], groups: [
            {
                id: 3,
                sender: 3,
                groupId: 1,
                content: 'hello everyone',
                contentType: 'STRING',
                datetime: new Date(),
                totalRead: 3
            }
        ]
    },
    chatList: [
        {
            userId: 1,
            msgId: 1
        },
        {
            userId: 2,
            msgId: 2
        },
        {
            groupId: 2,
            messageId: 3
        }
    ],

}

export default class SocketControl extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {
                userList: [],
                groupList: [],
                chatList: [],
                msgList: []
            },
            failedMsg: [],
            connectionStatus: ""
        };
        this.socket = {};
        this.pingTimeoutId = "";
        this.maxMsgId = 0;
        window.connectSocket = this.connect;
        this.connect = this.connect.bind(this);
        this.handleMsg = this.handleMsg.bind(this);
    }

    connect() {
        console.log('INIT!')
        const url = "ws://" + window.location.host;
        this.socket = new WebSocket(url);
        socket.onopen = e => {
            this.setState({ connectionStatus: "OK" });
        };
        socket.onmessage = e => {
            this.handleMessage(e);
        };
        socket.onerror = e => {
            console.error("Websocket error:" + e);
        };
    }


    resetPingTimeout() {
        const pingTimeout = 30000 + 1000;
        clearTimeout(this.pingTimeoutId);
        this.pingTimeoutId = setTimeout(() => {
            this.setState({connectionStatus: 'NOT_OK'});
            this.socket.close();
        }, pingTimeout);
    }

    routeMsg(routeMsg) {
        switch (routeMsg.route) {
            case '/chat-list':
                const chatList = routeMsg.msg;
                this.setState(() => {
                    return {data: {chatList}}
                })
            case "/chat/user/?":
                break;
            case "/chat/group/?":
                break;
            case "/user-list":
                const userList = routeMsg.msg;
                this.setState({data: {userList}});
                break;
            case '/user/?':
                break;
            case '/group-list':
                const groupList = routeMsg.msg;
                this.setState({data: {groupList}});
                break;
            case "/msg-list":
                break;
            case "/msg/user/?":
                this.setState(({data: {msgList}})=> {
                    const msg = routeMsg.msg;
                    let newMsgList;
                    if (!msg.id) {
                        //bikin msgId buat msg yg baru dikirim tapi blm di tulis ke db
                        newMsgList = Object.assign(msgList,{['NEW-'+this.maxMsgId++]: msg});
                    } else {
                        newMsgList = Object.assign(msgList,{[msg.id]: msg})
                    }
                    return Object.assign({data}, {data: {msgList: newMsgList}})
                })
                break;
            case "/ping":
                this.setState({ connectionStatus: "OK" });
                this.resetPingTimeout();
                break;
            default:
                break;
        }
    }

    handleMsg(e) {
        const msgs = e.data;
        if (Array.isArray(msgs)) {
            msgs.forEach((msg)=> {
                this.routeMessage(msg);
            })
        } else this.routeMessage(msgs);
    }

    sendMsg(msg) {
        this.socket.send(msg);
    }
    
    componentDidMount() {
        
    }

    render() {
        const {connectionStatus, data} = this.state;
        return (
            <div>
                <ChatControl
                    userList={data.userList}
                    groupList={data.groupList}
                    msgList={data.msgList}
                    chatList={data.chatList}
                />
            </div>
        );
    }
}