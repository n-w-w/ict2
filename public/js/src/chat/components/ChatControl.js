import React from 'react';
import PropTypes from 'prop-types';
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import MediaQuery from 'react-responsive';

export default class ChatControl extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    findChat({userId, groupId}) {
        const chatList = this.props.chatList;
        let chat;
        if (userId) {
            chat = chatList.find(chat => (chat.userId == userId))
        } else if (groupId) {
            chat = chatList.find(chat => (chat.groupId == groupId))
        }
        return chat;
    }

    findData(dataType, dataId) {
        const {userList, groupList, msgList} = this.props;
        switch (dataType) {
            case 'USER':
                return userList.find(user => (user.userId == dataId));
            case 'GROUP':
                return groupList.find(group => (group.groupId == dataId));
            case 'MSG':
                return msgList.find(msg => (msg.msgId == dataId));
            case 'GROUP_CHAT':
                return this.findChat({groupId: dataId});
            case 'USER_CHAT':
                return this.findChat({userId: dataId});
            default:
                break;
        }
    }

    render() {
        const {userList, groupList, msgList, chatList} = this.props;
        const SCREEN_BREAKPOINT = 768;
        return (
            <div>
                <Router>
                    <Link to="/contact-list">Contacts</Link>
                    <Link to="/chat-list">Chats</Link>
                    <Switch>
                        <Route path="/contact-list" render={props => (
                            <ContactList userList={userList} groupList={groupList} {...props} />
                        )} />
                        <Route path="/chat-list" render={props => (
                            <ChatList chatList={chatList} findData={this.findData} {...props} />
                        )} />
                        <MediaQuery maxWidth={SCREEN_BREAKPOINT}>
                            <Route path="/chat/:id" render={props => (
                                <ChatRoom msgList={msgList} findData={this.findData} {...props} />
                            )}
                            />
                        </MediaQuery>
                    </Switch>
                    <MediaQuery minWidth={SCREEN_BREAKPOINT}>
                        <Route path="/chat/:id"/>
                    </MediaQuery>

                </Router>
            </div>
        )
    }
}

ChatControl.propTypes = {
    userList: PropTypes.arrayOf(PropTypes.shape({
        userId: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
        avatarLink: PropTypes.string
    })),
    groupList: PropTypes.arrayOf(PropTypes.shape({
        groupId: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
        avatarLink: PropTypes.string
    })),
    msgList: PropTypes.arrayOf(PropTypes.shape({
        msgId: PropTypes.number.isRequired,
        senderId: PropTypes.number.isRequired,
        receiverId: PropTypes.number,
        groupId: PropTypes.number,
        content: PropTypes.string.isRequired,
        contentType: PropTypes.oneOf(['STRING', 'IMG_LINK', 'VID_LINK']),
        datetime: PropTypes.instanceOf(Date),
        numRead: PropTypes.number
    })),
    chatList: PropTypes.arrayOf(PropTypes.shape({
        userId: PropTypes.number,
        groupId: PropTypes.number,
        lastMsgId: PropTypes.number,
        numUnreadMsg: PropTypes.number
    })),


}