const Database = require('../models/database');
const db = new Database();
const {getPayload} = require('../helpers/token-auth');

module.exports.view = function(req,res,next) {
    res.render('pages/user/index');
}

module.exports.viewLogin = function(req,res,next) {
    res.render('pages/user/login');
}

module.exports.tokenSignin = async function(req,res,next) {
    res.setHeader('Access-Control-Allow-Credentials', 'true')

    console.log("verifying token")
    const authorizedHostDomain = 'ict2.org';
    console.dir(req.body)
    const token = req.body.idToken;
    if(!token) return console.log('token g bnr:'+ token);
    const payload = await getPayload(token)
    const {email, hd, name} = payload;

    if (hd != authorizedHostDomain) {console.log('Email g bnr')}
    console.log('email aman')
    const userRecord = await db.helpers.user.findOne({email});
    console.log('userRecord:')
    console.dir(userRecord);
    let userId;
    if (!userRecord) {
        userId = await db.helpers.user.create({email, name})}
    userId = userRecord.id;
    req.session.userId = userId;
    console.log( req.session.id)
    res.send({sid: req.session.id})
    console.log('created userId session: ' + req.session.userId);
}