const {OAuth2Client} = require("google-auth-library");
const {oauth2} = require('../../config');
const client = new OAuth2Client(oauth2.client_id);

async function getPayload(token) {
    return new Promise(async (resolve, reject) => {
        const ticket = await client.verifyIdToken({
            idToken: token,
            audience: oauth2.client_id
        }).catch(error => reject(error));
        const payload = ticket.getPayload();
        resolve(payload)
    });
}

module.exports.getPayload = getPayload;
