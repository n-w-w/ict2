const Group = require('./Group');

module.exports = class Message {

    constructor(query) {
        this.query = query;
        this.helpers = {
            group: new Group(this.query)
        };
    }


    async createPersonMsg({senderId, content, type, datetime, receiverId}) {
        const {insertId: messageId} = await this.query(
            `
                INSERT INTO message
                SET ?
            `, {sender_id: senderId, content, type, datetime}
        )
        await this.query(
            `
                INSERT INTO message_receiver
                SET ?
            `, {message_id: messageId, receiver_id: receiverId}
        )
    }

    async createGroupMsg({senderId, groupId, content, type, datetime}) {
        const {insertId: messageId} = await this.query(
            `
                INSERT INTO message
                SET ?
            `, {sender_id: senderId, group_id: groupId, content, type, datetime}
        );
        const members = await this.helpers.group.findMembers({groupId});

        members.forEach(async member => {
            await this.query(
                `
                    INSERT INTO message_receiver
                    SET ?
                `, {message_id: messageId,receiver_id: member.id}
            );
        });
    }

    async findMsgs({userId, otherUserId, groupId}) {
        const sql = {
            select: `
                m.sender_id AS senderId,
                m.group_id AS groupId,
                m.id AS id,
                m.content,
                m.type,
                m.datetime,
                mr.receiver_id AS receiverId
            `, from: `
                message AS m
                INNER JOIN message_receiver AS mr ON m.id=mr.message_id
            `, order: `
                m.datetime DESC
            ` 
        }
        let results = [];
        if (groupId == 'ANY') {
            return await this.query(
                `
                    SELECT ${sql.select}
                    FROM ${sql.from}
                    WHERE group_id IS NOT NULL
                    ORDER BY ${sql.order}
                `
            );
        } else if (groupId) {
            return await this.query(
                `
                    SELECT ${sql.select}
                    FROM ${sql.from}
                    WHERE group_id = ?
                    ORDER BY${sql.order}
                `, [groupId]
            )
        } else if(userId && (otherUserId == 'ANY' || !otherUserId)) {
            return await this.query(
                `
                    SELECT ${sql.select}
                    FROM ${sql.from}
                    WHERE
                        group_id IS NULL AND
                        (m.sender_id = ? OR mr.receiver_id = ?)
                    ORDER BY ${sql.order}
                `, [userId, userId]
            );
        }
        
        else if (userId && otherUserId) {
            //find messages in a personal chat of user and otheruser
            return await this.query(
                `
                    SELECT ${sql.select}
                    FROM ${sql.from}
                    WHERE
                        group_id IS NULL &&
                        (m.sender_id = ? OR mr.receiver_id = ?) &&
                        (m.sender_id = ? OR mr.receiver_id = ?)
                    ORDER BY ${sql.order}
                `, [userId, userId, otherUserId, otherUserId]);
        }
    }

    async findLastMsgList({userId}) {
        const personalMsgs = await this.findMsgs({userId})
        const groupMsgs = await this.findMsgs({userId, groupId: 'ANY'});
        const msgs = [...personalMsgs, ...groupMsgs]
        
        let chatUserIds = [];
        let chatGroupIds = [];
        const chatList = msgs.map((msg) => {
            let chat = {
                lastMsgContent: msg.content,
                datetime: msg.datetime
            }
            if (msg.groupId) {
                chat.groupId = msg.groupId;
                if(!chatGroupIds.includes(chat.groupId)) {
                    chatGroupIds.push(chat.groupId);
                    return chat;
                }
            }
            
            else {
                //msg.userId is the anotheruser that have chatted with user
                if (msg.receiverId==userId) {
                    chat.userId = msg.senderId;
                }
                else {
                    chat.userId = msg.receiverId;
                }

                if(!chatUserIds.includes(chat.userId)) {
                    chatUserIds.push(chat.userId);
                    return chat;
                }
            }
        }).filter((message) => message);
        return chatList;
    }

    async findUnreadMsgList({userId}) {
        const tableSql = 
        `
            ict2_chat_app.message AS m
            INNER JOIN ict2_chat_app.message_receiver AS mr
            ON m.id = mr.message_id 
        `
        const personalListSql =
        `
            SELECT m.sender_id AS userId, count(*) AS numUnreadMsg
            FROM 
	            ${tableSql}
            WHERE
                m.group_id IS NULL
                && mr.message_is_read IS NULL
                && mr.receiver_id = ?
            GROUP BY sender_id;
        `
        const personalList = await this.query(personalListSql, [userId]);
        const  groupListSqlSql = 
        `
            SELECT group_id AS groupId, count(*) AS numUnreadMsg
            FROM 
                ${tableSql}
            WHERE
                m.group_id IS NOT NULL
                && message_is_read IS NULL
                && mr.receiver_id = ?
            GROUP BY group_id;
        `
        const groupList = await this.query(groupListSqlSql, [userId]);
        return [...personalList, ...groupList];
    }

    async findChatList({userId}) {
        const lastMsgList = await this.findLastMsgList({userId});
        const unreadMsgList = await this.findUnreadMsgList({userId});
        let chatList = lastMsgList;
        for(let i = 0; i < lastMsgList.length; i++) {
            for(let j = 0; j < unreadMsgList.length; j++) {
                if (lastMsgList[i].userId == unreadMsgList[j].userId) {
                    chatList[i].numUnreadMsg = unreadMsgList[j].numUnreadMsg;
                    break;
                }
            }
        }
        return chatList
    }
}