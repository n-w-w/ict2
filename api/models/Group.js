module.exports = class Group {
    constructor(query) {
        this.query = query;
    }

    create() {
        
    }


    async findMany({groupIds}) {
        const sql = `
        SELECT 
            g.id AS id,
            g.name AS name,
            GROUP_CONCAT(gu.id) AS memberIds
        FROM
            \`group\` AS g
            INNER JOIN group_user AS gu ON g.id = gu.group_id
        WHERE g.id IN (?)
        GROUP BY g.id, gu.group_id;
        `;
        return await this.query(sql, [String(groupIds)]);
        
    }

    async findMembers({groupIds}) {
        return await this.query(`
            SELECT 
                g.id AS groupId,
                user_id AS id,
                gu.role AS role
            FROM
                \`group\` AS g
                INNER JOIN group_user AS gu ON g.id = gu.group_id
            WHERE gu.group_id IN (?)
            ;
        `,[String(groupIds)]);
    }

    async addUser() {
        return await this.query(`
            INSERT INTO group_user 
        `)
    }

    kickUser() {

    }

    setUserRole() {

    }
}