const columns = `
    id,
    email,
    name,
    avatar_link AS avatarLink
`
module.exports = class User {
    constructor(query) {
        this.query = query;
    }

    async create({email, name}) 
    {
        await this.query('INSERT INTO user (email, name) VALUES (?,?)',[email, name])
    }

    async findOne({id,email}) {
        if (!isNaN(id)) {
            const sql = `
                SELECT ${columns}
                FROM user
                WHERE id=?
            `;
            const users = await this.query(sql,[id])
            return users[0];
        } else if (email) {
            const sql = `
                SELECT ${columns}
                FROM user
                WHERE email=?
            `;
            const users = await this.query(sql, [email])
            return users[0];
        }
    }

    async findMany({userIds, emails}) {
        
        if (userIds) {
            const sql = `
                SELECT ${columns}
                FROM users
                WHERE id = IN  (?)
            `;
            return await this.query(sql, [userIds.toString()]);
        } else if (emails) {
            const sql = `
                SELECT ${columns}
                FROM users
                WHERE email = IN  (?)
            `;
            return await this.query(sql, [emails.toString()]);
        }
    }
    

}