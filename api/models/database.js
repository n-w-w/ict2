const mysql = require('mysql');
const {database: databaseConfig} = require('../../config');
const Message = require('./Message');
const User = require('./User');
const Group = require('./Group');

class Database {
    constructor(config = databaseConfig) {
        this.connection = mysql.createConnection(config);
        this.query = this.query.bind(this);
        this.helpers = {
            user: new User(this.query),
            group: new Group(this.query),
            message: new Message(this.query),
        }
    }
    query(sql, args) {
        return new Promise((resolve, reject) => {
            this.connection.query(sql, args, (err, rows) => {
                if (err) return reject(err);
                resolve(rows);
            });
        });
    }
    close() {
        this.connection.end();
    }
}

module.exports = Database;
