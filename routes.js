const UserController = require('./api/controllers/UserController');
const ChatController = require('./api/controllers/ChatController')

module.exports = (app) => {
    app.get('/', UserController.view);
    app.post('/tokensignin', UserController.tokenSignin)

    app.get('/chat', ChatController.viewIndex)
}