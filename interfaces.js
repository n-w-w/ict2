module.exports.RouteMsg = function RouteMsg({route, params, msg}) {
    this.route = route;
    this.params = params;
    this.msg = msg;
}

module.exports.Msg = function Msg({
    senderId,
    groupId,
    msgId,
    content,
    type,
    datetime,
    receiverId
}) {
    this.senderId = senderId;
    this.groupId = groupId;
    this.msgId = msgId;
    this.content = content;
    this.type = type;
    this.datetime = datetime;
    this.receiverId = receiverId;
}