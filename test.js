const Database = require('./api/models/database');
const db = new Database();

(async function(){
    console.log('User Model');
    console.log('#findOne');
    const user = await db.helpers.user.findOne({email: 'naufal@ict2.org'});
    console.dir(user)

    console.log('Message Model')
    console.log('#findChatList');
    const x = await db.helpers.message.findChatList({userId: 1})
    console.dir(x)
    console.log('#findUnreadMsgList')
    const y = await db.helpers.message.findUnreadMsgList({userId: 1});
    console.dir(y);
    
    console.log('#findMsgs');
    const msgs = await db.helpers.message.findMsgs({userId: 1});
    /*
    msgs.reduce((dict, msg)=> {
        dict[msg.id] = msg;
    })
    console.dir(msgs)*/
})()
